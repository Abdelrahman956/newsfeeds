package com.linkdevelopment.newsfeedsapp.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.linkdevelopment.newsfeedsapp.models.NewsFeedsModel;
import com.linkdevelopment.newsfeedsapp.views.api.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NewsFeedsViewModel extends ViewModel {
    private MutableLiveData<List<NewsFeedsModel.Articles>> newsFeedsMutableLiveData;

    public LiveData<List<NewsFeedsModel.Articles>> getNewsFeeds(Context context) {

        if (newsFeedsMutableLiveData == null) {
            newsFeedsMutableLiveData = new MutableLiveData<>();
            loadNewsFeeds(context);
        }

        return newsFeedsMutableLiveData;
    }

    private void loadNewsFeeds(Context context) {
        Observable.zip(RetrofitClient.getInstance(context).getApi()
                        .getNewsFeeds("the-next-web", "533af958594143758318137469b41ba9"), RetrofitClient.getInstance(context).getApi()
                        .getNewsFeeds("associated-press", "533af958594143758318137469b41ba9"),
                (nextWeb, associatedPress) -> {
                    List<NewsFeedsModel.Articles> articlesList = new ArrayList<>();
                    articlesList.addAll(nextWeb.getArticles());
                    articlesList.addAll(associatedPress.getArticles());
//
                    return articlesList;
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<NewsFeedsModel.Articles>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<NewsFeedsModel.Articles> articles) {
                        newsFeedsMutableLiveData.setValue(articles);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
