package com.linkdevelopment.newsfeedsapp.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.linkdevelopment.newsfeedsapp.R;
import com.linkdevelopment.newsfeedsapp.viewmodels.NewsFeedsViewModel;
import com.linkdevelopment.newsfeedsapp.views.adapters.NewsFeedsAdapter;

public class HomeScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    RecyclerView newsFeedsRecycler;
    NewsFeedsViewModel newsFeedsViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        newsFeedsRecycler = findViewById(R.id.news_feeds_recycler);
        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.drawer_toggle_color));

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        newsFeedsViewModel = ViewModelProviders.of(this).get(NewsFeedsViewModel.class);

        newsFeedsViewModel.getNewsFeeds(this).observe(this, newsFeedsModel -> {
            newsFeedsRecycler.setAdapter(new NewsFeedsAdapter(this, newsFeedsModel));
        });


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.explore) {
            Toast.makeText(HomeScreenActivity.this, getString(R.string.explore), Toast.LENGTH_SHORT).show();

        } else if (id == R.id.live_chat) {

            Toast.makeText(HomeScreenActivity.this, getString(R.string.live_chat), Toast.LENGTH_SHORT).show();

        } else if (id == R.id.gallery) {

            Toast.makeText(HomeScreenActivity.this, getString(R.string.gallery), Toast.LENGTH_SHORT).show();

        } else if (id == R.id.wish_list) {

            Toast.makeText(HomeScreenActivity.this, getString(R.string.wish_list), Toast.LENGTH_SHORT).show();

        } else if (id == R.id.e_magazine) {

            Toast.makeText(HomeScreenActivity.this, getString(R.string.e_magazine), Toast.LENGTH_SHORT).show();

        }


        // drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
