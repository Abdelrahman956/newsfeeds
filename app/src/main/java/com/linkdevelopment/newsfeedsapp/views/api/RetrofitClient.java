package com.linkdevelopment.newsfeedsapp.views.api;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RetrofitClient {
    private static Context sContext;

    String BASE_URL = "https://newsapi.org/v1/";

    private static volatile RetrofitClient mInstance;
    private Retrofit retrofit;
    private static final String TAG = "ServiceGenerator";

    public RetrofitClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofit = new Retrofit.Builder()
                .client(providerOkHttpClient())
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .build();
    }

    public static OkHttpClient providerOkHttpClient() {
        OkHttpClient.Builder okhttpclient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);


        return okhttpclient.build();
    }

    public static RetrofitClient getInstance(Context context) {
        if (mInstance == null) {
            synchronized (RetrofitClient.class) {
                mInstance = new RetrofitClient();
                sContext = context;
            }
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }

}