package com.linkdevelopment.newsfeedsapp.views.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.linkdevelopment.newsfeedsapp.R;

public class NewsDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    TextView newsTitle, newsAuthor, newsDate, newsDescription;
    ImageView back, newsImage;
    String image, author, date, description, title, url;
    Button openWebsite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        newsTitle = findViewById(R.id.news_title);
        newsAuthor = findViewById(R.id.news_author);
        newsImage = findViewById(R.id.news_image);
        newsDate = findViewById(R.id.news_date);
        newsDescription = findViewById(R.id.news_description);
        openWebsite = findViewById(R.id.open_website);
        back = findViewById(R.id.back);

        Intent intent = getIntent();
        image = intent.getStringExtra("image");
        author = intent.getStringExtra("author");
        date = intent.getStringExtra("date");
        description = intent.getStringExtra("description");
        title = intent.getStringExtra("title");
        url = intent.getStringExtra("url");

        newsAuthor.setText(author);
        newsDate.setText(date);
        newsDescription.setText(description);
        newsTitle.setText(title);
        Glide.with(this).load(image).into(newsImage);

        openWebsite.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == openWebsite) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
        if (view == back) {
            finish();
        }
    }
}