package com.linkdevelopment.newsfeedsapp.views.api;

import com.linkdevelopment.newsfeedsapp.models.NewsFeedsModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface Api {

    @GET("articles")
    Observable<NewsFeedsModel> getNewsFeeds(@Query("source") String source, @Query("apiKey") String apiKey);

}
