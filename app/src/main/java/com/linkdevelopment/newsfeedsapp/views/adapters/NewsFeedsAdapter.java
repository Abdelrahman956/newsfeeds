package com.linkdevelopment.newsfeedsapp.views.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.linkdevelopment.newsfeedsapp.R;
import com.linkdevelopment.newsfeedsapp.models.NewsFeedsModel;
import com.linkdevelopment.newsfeedsapp.views.activities.NewsDetailsActivity;

import java.util.List;

public class NewsFeedsAdapter extends RecyclerView.Adapter<NewsFeedsAdapter.NewsFeedsViewHolder> {
    Context context;
    List<NewsFeedsModel.Articles> newsFeedsModel;

    public NewsFeedsAdapter(Context context, List<NewsFeedsModel.Articles> newsFeedsModel) {
        this.context = context;
        this.newsFeedsModel = newsFeedsModel;
    }

    @NonNull
    @Override
    public NewsFeedsAdapter.NewsFeedsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.news_feeds_row, parent, false);
        return new NewsFeedsViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull NewsFeedsAdapter.NewsFeedsViewHolder holder, int position) {
        holder.newsTitle.setText(newsFeedsModel.get(position).getTitle());
        holder.newsAuthor.setText("By " + newsFeedsModel.get(position).getAuthor());
        holder.newsDate.setText(newsFeedsModel.get(position).getPublishedAt());
        Glide.with(context).load(newsFeedsModel.get(position).getUrlToImage()).into(holder.newsImage);
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, NewsDetailsActivity.class);
            intent.putExtra("image", newsFeedsModel.get(position).getUrlToImage());
            intent.putExtra("author", "By " + newsFeedsModel.get(position).getAuthor());
            intent.putExtra("title", newsFeedsModel.get(position).getTitle());
            intent.putExtra("description", newsFeedsModel.get(position).getDescription());
            intent.putExtra("date", newsFeedsModel.get(position).getPublishedAt());
            intent.putExtra("url", newsFeedsModel.get(position).getUrl());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return newsFeedsModel.size();
    }

    public class NewsFeedsViewHolder extends RecyclerView.ViewHolder {
        TextView newsTitle, newsAuthor, newsDate;
        ImageView newsImage;

        public NewsFeedsViewHolder(@NonNull View itemView) {
            super(itemView);
            newsTitle = itemView.findViewById(R.id.news_title);
            newsAuthor = itemView.findViewById(R.id.news_author);
            newsDate = itemView.findViewById(R.id.news_date);
            newsImage = itemView.findViewById(R.id.news_image);

        }
    }
}